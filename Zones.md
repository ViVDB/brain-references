# Tronc (brain stem)

## Bulbe rachidien (Medulla)

## Pont (Pons)

## Mésencéphale (midbrain)

# Thalamus

# Hypothalamus

# Chiasma optique (optic chiasm)

Important to realize how _low_ it is compared to the brain, it is _under_ it actually.

# noyaux gris de la base (basal ganglia)

un tas de rassemblements de neurones (les noyaux) au milieu (la base) du cerveau.

## striatum

Le striatum, partie des basal ganglia, contient le noyau caudé et le noyau lenticulaire.

### noyau caudé (caudate nucleus)

### noyau lenticulaire (lenticular nucleus)

Le noyau lenticulaire lui-même contient le putamen et le globe pâle.

#### putamen

#### globe pâle (globus pallidus)

# Corps calleux (corpus callosum)

L'autoroute d'information entre les 2 hémisphères. Il s'agit de matière blanche, soit, un tas d'axones entre les 2 côtés. Vu que ce n'est qu'un tas d'axones, c'est assez mal défini, il forme une grosse arche mais on ne représente jamais qu'une coupe. Il y a une représentation plus ou moins correcte sur http://www.g2conline.org/2022.

# lobes

## frontal

## pariétal

## occipital

## temporal

## insulaire

Somehow, there is a second brain inside the first, and cortex folds inside itself. According to wikipedia :

It is a "portion of the cerebral cortex (...) believed to be involved in consciousness and play a role in diverse functions usually linked to emotion or the regulation of the body's homeostasis. These functions include compassion and empathy, perception, motor control, self-awareness, cognitive functioning, and interpersonal experience. "


# scissures

## interhémisphérique

## sylvienne (sulcus lateralis, lateral sulcus, sylvian fissure, lateral fissure, sillon latéral)

Some Mr Sylvius discovered it, but at some point someone decided he didn't like him end changed the name so we can remember it. With all the teachers who use it.

## rolandique (central sulcus, rolandic fissure, sillon central)

## calcarine (calcarine sulcus, scissure calcarine)

calcarine vient "d'éperon" en latin selon wikipédia.


Ce n'est pas un sillon de surface mais un pic dans le lobe occipital.


# hippocampe (hippocampus)

# amygdale (amygdala)

# ventricules (ventricles)

## latéraux

## troisième ventricule

## 4ème ventricule

## aqueduc de Sylvius/aqueduc cérébral


# sources


A lot of https://en.wikipedia.org and https://fr.wikipedia.org.

Plus, the images come from the course (GBIO0005-1, Uliège, 2019) and the links that can be accessed in the html
