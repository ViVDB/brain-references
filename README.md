## Zones.html

Html fait dans le cadre du cours d'introduction aux neurosciences cognitives (GBIO008) pour "voir" les différents moreaux du cerveau un peu mieux.

-----

## Zones.md

Zones.md devrait, à terme, si j'ai la patience de le compléter avant l'examen ce 21 juin 2019, contenir quelques infos de base sur chaque morceau du cerveau.

## schemes.odg

Et schemes.odg devrait, avec les mêmes conditions, reprendre un récap des images du cours et celles d'internet, sachant qu'on ne peut pas mettre de gif animé dedans, mais je ne connais pas de programme facile à utiliser qui les prenne en charge et soit cross-platform et facile à éditer facilement.

## backup

contient un backup des images utilisées, et l'html correspondant.

---

## Copyright


Copyright for the document : 2019 ViVDB

The images are not included in the copyright, I found them at the addresses that can be found in the html, mostly, and in my course.

Under Belgian law, as far as I understand it, they can be reproduced here, since it's for education :

"  3° la reproduction d'oeuvres, à l'exception des partitions musicales, à des fins d'illustration de l'enseignement ou de recherche scientifique, pour autant que l'utilisation soit justifiée par le but non lucratif poursuivi, et que l'utilisation ne porte pas préjudice à l'exploitation normale de l'oeuvre;"

(http://www.ejustice.just.fgov.be/cgi_loi/change_lg.pl?language=fr&la=F&table_name=loi&cn=2016122203)

(http://www.ejustice.just.fgov.be/eli/loi/2013/02/28/2013A11134/justel#t)

Now, there may be some images for which I have forgotten or lost the reference, in case of problem it is possible to add references or to close this repository to the public (make an issue).
